
FROM openjdk:8-jdk-alpine
EXPOSE 8082
VOLUME /tmp
ADD /target/spring-docker-spotify-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-Xmx750m", "-jar","/app.jar"]